angular.module('app', [])
    .service('addExpense', function () {
        var property = false;

        return {
            get: function () {
                return property;
            },
            set: function(value) {
                property = value;
            }
        };
    });